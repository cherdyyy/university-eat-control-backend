import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ReportsRepository } from '@features/reports/database/reports.repository';
import { ReportsEntity } from '@features/reports/database/reports.entity';
import { FoodHistoryEntity } from '@features/food-history/database/food-history.entity';
import { FindAllDto } from '@shared/dto/find-all.dto';
import { FindAllModel } from '@shared/models/find-all.model';
import { AppMailerService } from '@core/mailer/mailer.service';
import { ReportTemplate } from '@core/mailer/interfaces/report.interface';
import { UserRepository } from '@features/user/database/user.repository';
import { UserEntity } from '@features/user/database/user.entity';
import { CompanyEntity } from '@features/company/database/company.entity';
import { UserTypeEnum } from '@features/user/enums/user-type.enum';

@Injectable()
export class ReportsService {

  constructor(@InjectRepository(ReportsRepository)
              private reportsRepository: ReportsRepository,
              private mailerService: AppMailerService,
              @InjectRepository(UserRepository)
              private userRepository: UserRepository, ) {
  }

  createReport(foodHistoryList: FoodHistoryEntity): Promise<ReportsEntity> {
    return this.reportsRepository.save({
      foodHistoryList
    });
  }

  async getReportsList(query: FindAllDto): Promise<FindAllModel<ReportsEntity>> {
    return this.reportsRepository.findAll(query, [
      'foodHistoryList',
      'foodHistoryList.user',
      'foodHistoryList.food',
    ]);
  }

  async sendEmployeeReportMail(reportedUser: UserEntity, company: CompanyEntity): Promise<void> {
    try {
      const admins = await this.userRepository.find({
        type: UserTypeEnum.Admin,
        company: company
      })

      admins.forEach((admin) => {
        (async () => {
          await this.mailerService.sendMail({
            html: await AppMailerService.getTemplate<ReportTemplate>('report', {
              reportedUser: `${reportedUser.firstName} ${reportedUser.lastName}`,
              adminName: `${admin.firstName} ${admin.lastName}`,
              companyName: company.name
            }),
            to: admin.email,
            subject: 'Employee Report',
          });
        })()
      })
    } catch {}
  }
}
