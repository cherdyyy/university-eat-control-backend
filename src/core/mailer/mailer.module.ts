import { Global, Module } from '@nestjs/common';
import { AppMailerService } from './mailer.service';
import { MAILER_CONFIG } from '@core/mailer/mailer.config';
import { MailerModule } from '@nestjs-modules/mailer';

@Global()
@Module({
  providers: [
    AppMailerService,
  ],
  imports: [
    MailerModule.forRootAsync(MAILER_CONFIG),
  ],
  exports: [
    AppMailerService,
  ]
})
export class AppMailerModule {}
