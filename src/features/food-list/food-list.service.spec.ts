import { Test, TestingModule } from '@nestjs/testing';
import { FoodListService } from './food-list.service';

describe('FoodListService', () => {
  let service: FoodListService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [FoodListService],
    }).compile();

    service = module.get<FoodListService>(FoodListService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
