import { AppBaseRepository } from '@shared/database/repositories/base.repository';
import { EntityRepository } from 'typeorm';
import { PlanEntity } from '@features/plan/database/plan.entity';

@EntityRepository(PlanEntity)
export class PlanRepository extends AppBaseRepository<PlanEntity> {

  async getDefaultPlan(): Promise<PlanEntity> {
    return this.findOne({isDefault: true});
  }
}
