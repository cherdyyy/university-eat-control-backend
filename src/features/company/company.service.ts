import { Injectable } from '@nestjs/common';
import { DeepPartial, UpdateResult } from 'typeorm';
import { CompanyEntity } from '@features/company/database/company.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { PlanRepository } from '@features/plan/database/plan.repository';
import { CompanyRepository } from '@features/company/database/company.repository';
import { FoodHistoryService } from '@features/food-history/food-history.service';

@Injectable()
export class CompanyService {

  constructor(@InjectRepository(PlanRepository)
              private planRepository: PlanRepository,
              @InjectRepository(CompanyRepository)
              private companyRepository: CompanyRepository,
              private foodListService: FoodHistoryService) {
  }

  async createCompany(entity: DeepPartial<CompanyEntity>): Promise<CompanyEntity> {
    const company: DeepPartial<CompanyEntity> = {
      ...entity,
      plan: await this.planRepository.getDefaultPlan(),
    };

    return this.companyRepository.insertAndReturnOne(company);
  }

  async updateCompany(entity: DeepPartial<CompanyEntity>, company: CompanyEntity): Promise<UpdateResult> {
    return this.companyRepository.update(company.id, entity);
  }

  async getRecommendedBalance(company: CompanyEntity): Promise<number> {
    return this.foodListService.getRecommendedBalance(company);
  }
}
