import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { FindAllDto, Where, WhereTypeEnum } from '@shared/dto/find-all.dto';
import { EmployeeService } from '@features/employee/employee.service';
import { FindAllModel } from '@shared/models/find-all.model';
import { EmployeesListModel } from '@features/employee/models/employees-list.model';
import { JwtAuthGuard } from '@features/auth/guards/jwt-auth.guard';
import { User } from '@shared/decorators/user.decorator';
import { UserEntity } from '@features/user/database/user.entity';
import { EmployeeCreateDto } from '@features/employee/dto/create-employee.dto';
import { EmployeeModel } from '@features/employee/models/employee.model';

@UseGuards(JwtAuthGuard)
@Controller('employee')
export class EmployeeController {

  constructor(private employeeService: EmployeeService) {
  }

  @Post()
  async getEmployeesList(
    @Body() query: FindAllDto<EmployeesListModel>,
    @User() user: UserEntity
  ): Promise<FindAllModel<EmployeesListModel>> {
    if (!user.company) {
      return new FindAllModel([]);
    } else {
      query.where.push(
        new Where('company', user.company, WhereTypeEnum.SIMPLE)
      );
    }

    const result = await this.employeeService.getEmployeesList(query);

    return {
      ...result,
      items: result.items.map(e => new EmployeesListModel(e)),
    }
  }

  @Post('add')
  async createEmployee(
    @Body() body: EmployeeCreateDto,
    @User() user: UserEntity
  ): Promise<EmployeeModel> {
    return new EmployeeModel(await this.employeeService.createEmployee(body, user))
  }
}
