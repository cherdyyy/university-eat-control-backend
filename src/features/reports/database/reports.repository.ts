import { AppBaseRepository } from '@shared/database/repositories/base.repository';
import { EntityRepository } from 'typeorm';
import { ReportsEntity } from '@features/reports/database/reports.entity';

@EntityRepository(ReportsEntity)
export class ReportsRepository extends AppBaseRepository<ReportsEntity> {

}
