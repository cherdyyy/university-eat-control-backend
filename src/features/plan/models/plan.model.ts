import { CompanyEntity } from '@features/company/database/company.entity';
import { PlanEntity } from '@features/plan/database/plan.entity';
import { BaseModel } from '@shared/models/base.model';

export class PlanModel extends BaseModel {
  name: string;
  description: string;
  price: number;
  company: CompanyEntity;
  isDefault: boolean;

  constructor(entity: Partial<PlanEntity> = {}) {
    super(entity);
    this.name = entity.name;
    this.description = entity.description;
    this.price = entity.price;
    this.company = entity.company;
    this.isDefault = entity.isDefault;
  }
}
