import { MailerAsyncOptions } from '@nestjs-modules/mailer/dist/interfaces/mailer-async-options.interface';
import { AppConfigService } from '@core/config/config.service';
import { ConfigKeysEnum } from '@core/config/enums/config-keys.enum';
import { MailerOptions } from '@nestjs-modules/mailer/dist/interfaces/mailer-options.interface';

export const MAILER_CONFIG: MailerAsyncOptions = ({
  useFactory: (configService: AppConfigService): MailerOptions => ({
    transport: {
      host: configService.get(ConfigKeysEnum.MAIL_HOST),
      port: configService.get(ConfigKeysEnum.MAIL_PORT),
      service: configService.get(ConfigKeysEnum.MAIL_SERVICE),
      auth: {
        user: configService.get(ConfigKeysEnum.MAIL_LOGIN),
        pass: configService.get(ConfigKeysEnum.MAIL_PASSWORD),
      },
    },
    defaults: {
      from: `"${configService.get(ConfigKeysEnum.APP_NAME)}" <noreply@example.com>`
    }
  }),
  inject: [AppConfigService]
})
