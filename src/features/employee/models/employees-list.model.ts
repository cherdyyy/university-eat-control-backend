import { UserEntity } from '@features/user/database/user.entity';
import { BaseModel } from '@shared/models/base.model';
import { FoodListModel } from '@features/food-list/models/food-list.model';

export class EmployeesListModel extends BaseModel {
  login: string;
  firstName: string;
  lastName: string;
  address: string;
  phoneNumber: string;
  foodList: FoodListModel[]

  constructor(entity: UserEntity) {
    super(entity);
    this.login = entity.login;
    this.firstName = entity.firstName;
    this.lastName = entity.lastName;
    this.address = entity.address;
    this.phoneNumber = entity.phoneNumber;
    this.foodList = entity.foodList && entity.foodList.map(f => new FoodListModel(f))
  }
}
