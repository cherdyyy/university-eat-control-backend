import { EntityRepository } from 'typeorm';
import { FoodEntity } from '@features/food/database/food.entity';
import { AppBaseRepository } from '@shared/database/repositories/base.repository';

@EntityRepository(FoodEntity)
export class FoodRepository extends AppBaseRepository<FoodEntity> {

}
