import { AppBaseEntity } from '@shared/database/entities/base.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { FoodEntity } from '@features/food/database/food.entity';
import { CompanyEntity } from '@features/company/database/company.entity';

@Entity()
export class CompanyFoodListEntity extends AppBaseEntity {

  @ManyToOne(() => FoodEntity, food => food.companyFoodList)
  food: FoodEntity;

  @ManyToOne(() => CompanyEntity, company => company.foodList)
  company: CompanyEntity;

  @Column()
  count: number;
}
