import { Body, Controller, Post, UseGuards } from '@nestjs/common';
import { FindAllDto, Where, WhereTypeEnum } from '@shared/dto/find-all.dto';
import { User } from '@shared/decorators/user.decorator';
import { UserEntity } from '@features/user/database/user.entity';
import { FindAllModel } from '@shared/models/find-all.model';
import { ReportsService } from '@features/reports/reports.service';
import { ReportModel } from '@features/reports/models/report.model';
import { JwtAuthGuard } from '@features/auth/guards/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('reports')
export class ReportsController {

  constructor(private reportsService: ReportsService) {
  }

  @Post()
  async getReports(
    @Body() query: FindAllDto<ReportModel>,
    @User() user: UserEntity
  ): Promise<FindAllModel<ReportModel>> {
    if (!user.company) {
      return new FindAllModel([]);
    } else {
      query.where.push(
        new Where('company', user.company, WhereTypeEnum.SIMPLE)
      );
    }

    const result = await this.reportsService.getReportsList(query);

    return {
      ...result,
      items: result.items.map(e => new ReportModel(e)),
    }
  }
}
