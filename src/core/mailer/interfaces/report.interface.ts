export interface ReportTemplate {
  companyName: string;
  adminName: string;
  reportedUser: string;
  email?: string;
}
