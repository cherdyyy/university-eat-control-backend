import { Global, Module } from '@nestjs/common';
import { FoodHistoryService } from './food-history.service';
import { FoodHistoryController } from './food-history.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FoodHistoryRepository } from '@features/food-history/database/food-history.repository';

@Global()
@Module({
  providers: [
    FoodHistoryService
  ],
  controllers: [
    FoodHistoryController
  ],
  imports: [
    TypeOrmModule.forFeature([FoodHistoryRepository])
  ],
  exports: [
    FoodHistoryService,
    TypeOrmModule
  ]
})
export class FoodHistoryModule {}
