import { IsString } from 'class-validator';

export class FoodDeleteDto {
  @IsString()
  ids: string;
}
