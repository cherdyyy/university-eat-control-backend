import { FoodHistoryEntity } from '@features/food-history/database/food-history.entity';
import { format } from 'date-fns';

export class DashboardModel {
  totalMoneySpent = 0;
  totalFood = 0;
  moneySpent = {};
  topEmployees = {};
  history: DashboardHistory[] = [];
  activity = {};
  employeesCount = 0;
  reportsCount = 0;

  constructor(history: FoodHistoryEntity[]) {
    const topEmployees = {};
    const uniqueEmployees = new Set();

    for (const item of history) {
      const historyDate = format(item.createdAt, 'dd.MM.yyyy');
      this.totalMoneySpent += (item.price * item.count);
      this.totalFood += item.count;
      this.reportsCount += item.reportsCount;

      // money spent by day
      this.moneySpent[historyDate] = this.moneySpent[historyDate] ?
        this.moneySpent[historyDate] + (item.price * item.count) : (item.price * item.count);

      // top employee by count
      topEmployees[item.user.id] = {
        count: topEmployees[item.user.id] ?
          topEmployees[item.user.id].count + item.count : item.count,
        price: topEmployees[item.user.id] ?
          topEmployees[item.user.id].price + (item.price * item.count) : (item.price * item.count),
        userFullName: `${item.user.firstName} ${item.user.lastName}`,
      };

      // activity by day
      this.activity[historyDate] = this.activity[historyDate] ?
        this.activity[historyDate] + item.count : item.count;

      uniqueEmployees.add(item.user.id);
    }
    this.employeesCount = uniqueEmployees.size;
    // top employee by count
    Object.entries(topEmployees)
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      .sort(([, valueA], [, valueB]) => valueA.price - valueB.price)
      .slice(-3)
      .forEach(([key, value]) => {
        this.topEmployees[key] = value;
      });


    this.history = history.slice(-4)
      .map((h) => new DashboardHistory(h));
  }
}

export class DashboardHistory {
  userFullName: string;
  foodName: string;
  count: number;

  constructor(history: FoodHistoryEntity) {
    this.userFullName = `${history.user.firstName} ${history.user.lastName}`;
    this.foodName = history.food.name;
    this.count = history.count;
  }
}
