import { AppBaseEntity } from '@shared/database/entities/base.entity';
import { Column, Entity, ManyToOne } from 'typeorm';
import { UserEntity } from '@features/user/database/user.entity';
import { FoodEntity } from '@features/food/database/food.entity';
import { ReportsEntity } from '@features/reports/database/reports.entity';

@Entity()
export class FoodListEntity extends AppBaseEntity {

  @ManyToOne(() => UserEntity, user => user.foodHistory)
  user: UserEntity;

  @ManyToOne(() => FoodEntity, food => food.foodHistory, { eager: true })
  food: FoodEntity;

  @Column()
  count: number;
}
