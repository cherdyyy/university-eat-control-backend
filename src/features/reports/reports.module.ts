import { Global, Module } from '@nestjs/common';
import { ReportsController } from './reports.controller';
import { ReportsService } from './reports.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ReportsRepository } from '@features/reports/database/reports.repository';

@Global()
@Module({
  controllers: [ReportsController],
  providers: [ReportsService],
  imports: [TypeOrmModule.forFeature([ReportsRepository])],
  exports: [
    ReportsService,
    TypeOrmModule
  ]
})
export class ReportsModule {}
