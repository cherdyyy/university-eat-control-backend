import { Global, Module } from '@nestjs/common';
import { PlanService } from './plan.service';
import { PlanController } from './plan.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { PlanRepository } from '@features/plan/database/plan.repository';

@Global()
@Module({
  providers: [
    PlanService
  ],
  controllers: [
    PlanController
  ],
  imports: [
    TypeOrmModule.forFeature([PlanRepository]),
  ],
  exports: [
    TypeOrmModule
  ]
})
export class PlanModule {}
