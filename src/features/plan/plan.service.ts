import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { PlanRepository } from '@features/plan/database/plan.repository';
import { PlanEntity } from '@features/plan/database/plan.entity';

@Injectable()
export class PlanService {

  constructor(@InjectRepository(PlanRepository)
              private planRepository: PlanRepository) {
  }

  async getPlans(): Promise<PlanEntity[]> {
    return this.planRepository.find();
  }
}
