import { AppBaseRepository } from '@shared/database/repositories/base.repository';
import { EntityRepository } from 'typeorm';
import { CompanyFoodListEntity } from '@features/company-food-list/database/company-food-list.entity';
import { CompanyEntity } from '@features/company/database/company.entity';

@EntityRepository(CompanyFoodListEntity)
export class CompanyFoodListRepository extends AppBaseRepository<CompanyFoodListEntity> {

  getCompaniesFoodList(company: CompanyEntity): Promise<CompanyFoodListEntity[]> {
    return this.createQueryBuilder('company_food_list_entity')
      .where({company})
      .leftJoinAndSelect('company_food_list_entity.food', 'food')
      .getMany();
  }
}
