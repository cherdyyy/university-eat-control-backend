import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CompanyFoodListRepository } from '@features/company-food-list/database/company-food-list.repository';
import { CompanyFoodListEntity } from '@features/company-food-list/database/company-food-list.entity';
import { CompanyEntity } from '@features/company/database/company.entity';
import { CompanyFoodListDto } from '@features/company-food-list/dto/company-food-item.dto';
import { IdDto } from '@shared/dto/id.dto';
import { DeleteResult } from 'typeorm/query-builder/result/DeleteResult';

@Injectable()
export class CompanyFoodListService {

  constructor(@InjectRepository(CompanyFoodListRepository)
              private foodRepository: CompanyFoodListRepository) {
  }

  async addCompanyFood(
    company: CompanyEntity,
    { list }: CompanyFoodListDto,
  ): Promise<void> {
    await this.foodRepository.insert(
      list.map(item => ({
        ...item,
        company,
      }))
    )
  }

  getCompanyFoodList(company: CompanyEntity): Promise<CompanyFoodListEntity[]>  {
    return this.foodRepository.getCompaniesFoodList(company);
  }

  deleteCompanyFoodList(body: IdDto): Promise<DeleteResult>  {
    return this.foodRepository.delete(body);
  }
}
