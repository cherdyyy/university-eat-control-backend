import { AppBaseRepository } from '@app/shared/database/repositories/base.repository';
import { UserEntity } from '@app/features/user/database/user.entity';
import { EntityRepository } from 'typeorm';

@EntityRepository(UserEntity)
export class UserRepository extends AppBaseRepository<UserEntity> {

  async getFullUser(id: number): Promise<UserEntity> {
    return await this.createQueryBuilder('user_entity')
      .leftJoinAndSelect('user_entity.company', 'user_company')
      .leftJoinAndSelect('user_company.plan', 'user_company_plan')
      .where('user_entity.id = :id', {id})
      .getOne();
  }
}
