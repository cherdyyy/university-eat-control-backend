export interface SignUpTemplate {
  email: string;
  password: string;
  name: string;
  companyName: string;
}
