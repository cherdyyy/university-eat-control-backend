import { Column, Entity, ManyToOne, OneToMany } from 'typeorm';
import { AppBaseEntity } from '@shared/database/entities/base.entity';
import { CompanyEntity } from '@features/company/database/company.entity';
import { FoodHistoryEntity } from '@features/food-history/database/food-history.entity';
import { FoodListEntity } from '@features/food-list/database/food-list.entity';
import { CompanyFoodListEntity } from '@features/company-food-list/database/company-food-list.entity';
import { FoodCategoryEntity } from '@features/food-category/database/food-category.entity';

@Entity()
export class FoodEntity extends AppBaseEntity {

  @Column()
  name: string;

  @Column()
  description: string;

  @Column({ default: 0 })
  price: number;

  @Column({ default: 0 })
  calories: number;

  @ManyToOne(() => CompanyEntity)
  company: CompanyEntity;

  @OneToMany(() => FoodHistoryEntity, history => history.food)
  foodHistory: FoodHistoryEntity[];

  @OneToMany(() => FoodListEntity, list => list.food)
  foodList: FoodListEntity[];

  @OneToMany(() => CompanyFoodListEntity, company => company.food)
  companyFoodList: CompanyFoodListEntity;

  @ManyToOne(() => FoodCategoryEntity)
  category: FoodCategoryEntity;
}
