import { Body, Controller, Delete, Get, Param, Patch, Post, UseGuards } from '@nestjs/common';
import { FoodDto } from '@features/food/dto/food.dto';
import { FoodModel } from '@features/food/models/food.model';
import { FoodService } from '@features/food/food.service';
import { StatusModel } from '@shared/models/status.model';
import { IdDto } from '@shared/dto/id.dto';
import { User } from '@shared/decorators/user.decorator';
import { UserEntity } from '@features/user/database/user.entity';
import { JwtAuthGuard } from '@features/auth/guards/jwt-auth.guard';
import { FindAllModel } from '@shared/models/find-all.model';
import { FindAllDto, Where, WhereTypeEnum } from '@shared/dto/find-all.dto';
import { FoodEntity } from '@features/food/database/food.entity';
import { OptionListModel } from '@shared/models/option-list.model';

@UseGuards(JwtAuthGuard)
@Controller('food')
export class FoodController {

  constructor(private foodService: FoodService) {}

  @Get('list')
  async getFoodList(): Promise<OptionListModel[]> {
    const result = await this.foodService.getFoodList();

    return result.map(f => new OptionListModel(f.id, f.name));
  }

  @Post('add')
  async createFood(@Body() body: FoodDto, @User() user: UserEntity): Promise<void> {
    await this.foodService.createFood(body, user.company)
  }

  @Patch()
  async updatePartial(@Body() body: IdDto): Promise<StatusModel> {
    await this.foodService.updateFoodPartial(body);
    return new StatusModel(true);
  }

  @Get(':id')
  async getFood(@Param() params: IdDto): Promise<FoodModel> {
    return new FoodModel(await this.foodService.getFoodById(params.id));
  }

  @Delete()
  async deleteProduct(@Body() body: IdDto): Promise<StatusModel> {
    return this.foodService.deleteProduct(body.id);
  }

  @Post()
  async getFoodListPagination(
    @User() user: UserEntity,
    @Body() query: FindAllDto<FoodEntity>,
  ): Promise<FindAllModel<FoodModel>> {
    if (!user.company) {
      return new FindAllModel([]);
    } else {
      query.where.push(
        new Where('company', user.company, WhereTypeEnum.SIMPLE)
      );
    }

    const result = await this.foodService.getFoodListPagination(query);

    return {
      ...result,
      items: result.items.map(e => new FoodModel(e)),
    };
  }
}
