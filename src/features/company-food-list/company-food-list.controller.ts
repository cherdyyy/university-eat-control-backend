import { Body, Controller, Delete, Get, Param, Post, UseGuards } from '@nestjs/common';
import { User } from '@shared/decorators/user.decorator';
import { UserEntity } from '@features/user/database/user.entity';
import { CompanyFoodListService } from '@features/company-food-list/company-food-list.service';
import { JwtAuthGuard } from '@features/auth/guards/jwt-auth.guard';
import { CompanyFoodListModel } from '@features/company-food-list/models/company-food-list.model';
import { CompanyFoodListDto } from '@features/company-food-list/dto/company-food-item.dto';
import { StatusModel } from '@shared/models/status.model';
import { IdDto } from '@shared/dto/id.dto';

@UseGuards(JwtAuthGuard)
@Controller('company-food-list')
export class CompanyFoodListController {

  constructor(private companyFoodListService: CompanyFoodListService) {
  }

  @Post()
  async addCompanyFoodList(
    @User() user: UserEntity,
    @Body() body: CompanyFoodListDto
  ): Promise<StatusModel> {
    await this.companyFoodListService.addCompanyFood(user.company, body);
    return new StatusModel(true);
  }

  @Get()
  async getCompanyFoodList(
    @User() user: UserEntity
  ): Promise<CompanyFoodListModel[]> {
    const foodList = await this.companyFoodListService.getCompanyFoodList(user.company);
    return foodList.map(food => new CompanyFoodListModel(food));
  }

  @Delete(':id')
  async deleteCompanyFood(
    @Param() params: IdDto
  ): Promise<StatusModel> {
    await this.companyFoodListService.deleteCompanyFoodList(params);
    return new StatusModel(true);
  }
}
