import { Global, Module } from '@nestjs/common';
import { FoodCategoryController } from './food-category.controller';
import { FoodCategoryService } from './food-category.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FoodCategoryRepository } from '@features/food-category/database/food-category.repository';

@Global()
@Module({
  controllers: [FoodCategoryController],
  providers: [FoodCategoryService],
  imports: [
    TypeOrmModule.forFeature([FoodCategoryRepository]),
  ],
  exports: [
    FoodCategoryService,
    TypeOrmModule
  ]
})
export class FoodCategoryModule {}
