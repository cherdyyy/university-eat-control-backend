import { Body, Controller, Get, Post, UseGuards, Patch, Delete, Query } from '@nestjs/common';
import { StatusModel } from '@shared/models/status.model';
import { FoodListService } from '@features/food-list/food-list.service';
import { User } from '@shared/decorators/user.decorator';
import { UserEntity } from '@features/user/database/user.entity';
import { FoodListDto } from '@features/food-list/dto/food-list.dto';
import { FoodListModel } from '@features/food-list/models/food-list.model';
import { JwtAuthGuard } from '@features/auth/guards/jwt-auth.guard';
import { TakeFoodDto } from '@features/food-list/dto/take-food.dto';
import { FoodDeleteDto } from '@features/food-list/dto/food-delete.dts';

@UseGuards(JwtAuthGuard)
@Controller('food-list')
export class FoodListController {

  constructor(private foodListService: FoodListService) {
  }

  @Post()
  async addUserFood(
    @Body() body: FoodListDto
  ): Promise<StatusModel> {
    await this.foodListService.addUserFood(body.user, body);
    return new StatusModel(true);
  }

  @Get()
  async getUserFoodList(@User() user: UserEntity): Promise<FoodListModel[]> {
    const foodList = await this.foodListService.getUserFoodList(user);
    return foodList.map(food => new FoodListModel(food));
  }

  @Patch()
  async takeFood(
    @User() user: UserEntity,
    @Body() body: TakeFoodDto
  ): Promise<FoodListModel> {
    return new FoodListModel(await this.foodListService.takeFood(user, body));
  }

  @Delete()
  async deleteUserFood(
    @Query() body: FoodDeleteDto
  ): Promise<StatusModel> {
    const ids = body.ids.split(',').map(i => +i);
    await this.foodListService.deleteUserFood(ids);
    return new StatusModel(true);
  }
}
