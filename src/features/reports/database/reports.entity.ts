import { AppBaseEntity } from '@app/shared/database/entities/base.entity';
import { Entity, ManyToOne } from 'typeorm';
import { FoodHistoryEntity } from '@features/food-history/database/food-history.entity';
import { CompanyEntity } from '@features/company/database/company.entity';

@Entity()
export class ReportsEntity extends AppBaseEntity {

  @ManyToOne(() => FoodHistoryEntity, list => list.reports)
  foodHistoryList: FoodHistoryEntity;

  @ManyToOne(() => CompanyEntity, company => company.reports)
  company: CompanyEntity;
}
