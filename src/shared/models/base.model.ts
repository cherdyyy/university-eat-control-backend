import { AppBaseEntity } from '@shared/database/entities/base.entity';

export class BaseModel {
  id: number;
  createdAt: Date;
  updatedAt: Date;
  // version: number;

  constructor(entity: Partial<AppBaseEntity> = {}) {
    this.id = entity.id;
    this.createdAt = entity.createdAt;
    this.updatedAt = entity.updatedAt;
    // this.version = entity.version;
  }
}
