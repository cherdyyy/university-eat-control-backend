import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from '@features/user/database/user.repository';
import { FindAllDto } from '@shared/dto/find-all.dto';
import { FindAllModel } from '@shared/models/find-all.model';
import { UserEntity } from '@features/user/database/user.entity';
import { EmployeeCreateDto } from '@features/employee/dto/create-employee.dto';
import { hash } from 'bcrypt';
import { UserTypeEnum } from '@features/user/enums/user-type.enum';
import { AuthResponseTextEnum } from '@features/auth/enums/response-text.enum';
import { randomStr } from '@shared/helpers/random-str.helper';
import { AuthService } from '@features/auth/auth.service';

@Injectable()
export class EmployeeService {

  constructor(@InjectRepository(UserRepository)
              private usersRepository: UserRepository,
              private authService: AuthService) {
  }

  async getEmployeesList(query: FindAllDto): Promise<FindAllModel<UserEntity>> {
    return this.usersRepository.findAll(query, ['foodList']);
  }

  async createEmployee(body: EmployeeCreateDto, user: UserEntity): Promise<UserEntity> {
    const findUser = await this.usersRepository.findOne({
      email: body.email,
    });

    if (findUser) {
      throw new HttpException(
        AuthResponseTextEnum.USER_EXIST,
        HttpStatus.UNAUTHORIZED,
      );
    }

    const password = randomStr();
    await this.authService.sendUserCreateMail({
      ...body,
      password,
      companyName: user.company.name,
      name: `${body.firstName} ${body.lastName}`,
    });

    return await this.usersRepository.insertAndReturnOne({
      ...body,
      password: await hash(password, 10),
      type: UserTypeEnum.Employee,
      company: user.company,
    });
  }
}
