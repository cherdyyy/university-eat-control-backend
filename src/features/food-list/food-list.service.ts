import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FoodListRepository } from '@features/food-list/database/food-list.repository';
import { UserEntity } from '@features/user/database/user.entity';
import { FoodListDto } from '@features/food-list/dto/food-list.dto';
import { FoodListEntity } from '@features/food-list/database/food-list.entity';
import { TakeFoodDto } from '@features/food-list/dto/take-food.dto';
import { FoodHistoryService } from '@features/food-history/food-history.service';
import { FoodListResponseTextEnum } from '@features/food-list/enums/response-text.enum';
import { ReportsService } from '@features/reports/reports.service';

@Injectable()
export class FoodListService {

  constructor(@InjectRepository(FoodListRepository)
              private foodRepository: FoodListRepository,
              private foodHistoryService: FoodHistoryService,
              private reportsService: ReportsService) {
  }

  async addUserFood(
    user: UserEntity,
    foodList: FoodListDto
  ): Promise<void> {
    await this.foodRepository.insert(
      foodList.list.map(food => ({
        ...food,
        user
      }))
    );
  }

  async deleteUserFood(
    foodList: number[]
  ): Promise<void> {
    await this.foodRepository.delete(foodList);
  }

  async getUserFoodList(user: UserEntity): Promise<FoodListEntity[]> {
    return this.foodRepository.getAllUserFood(user);
  }

  async takeFood(user: UserEntity, body: TakeFoodDto): Promise<FoodListEntity> {
    const userFood = await this.foodRepository.findOne({
      user,
      food: body.food,
    }, {
      relations: ['food']
    });

    if (!userFood) {
      throw new HttpException(
        FoodListResponseTextEnum.FOOD_ITEM_NOT_FOUND,
        HttpStatus.NOT_ACCEPTABLE
      )
    }

    userFood.count = userFood.count - body.count;

    await this.foodRepository.update({food: body.food}, {
      count: userFood.count
    });

    const history = await this.foodHistoryService.addHistoryItem({
      food: body.food,
      user,
      count: body.count,
      company: user.company,
      price: userFood.food.price
    });

    if (userFood.count <= 0) {
      await this.reportsService.createReport(history);
      this.reportsService.sendEmployeeReportMail(user, user.company);
    }

    return userFood;
  }
}
