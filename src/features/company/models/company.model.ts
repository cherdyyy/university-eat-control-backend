import { CompanyEntity } from '@features/company/database/company.entity';
import { BaseModel } from '@shared/models/base.model';
import { PlanModel } from '@features/plan/models/plan.model';

export class CompanyModel extends BaseModel {
  name: string;
  address: string;
  plan: PlanModel;
  employeesCount: number;
  balance: number;

  constructor(entity: Partial<CompanyEntity> = {}) {
    super(entity);
    this.name = entity.name;
    this.address = entity.address;
    this.employeesCount = entity.employeesCount;
    this.plan = new PlanModel(entity.plan);
    this.balance = entity.balance;
  }
}
