import { IsEmail, IsNotEmpty, IsPhoneNumber, IsString } from 'class-validator';

export class EmployeeCreateDto {

  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsNotEmpty()
  @IsString()
  lastName: string;

  @IsString()
  @IsNotEmpty()
  address: string;

  @IsPhoneNumber(null)
  phoneNumber: string;

  @IsEmail()
  email: string;
}
