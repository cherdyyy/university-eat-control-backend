import { AppBaseRepository } from '@shared/database/repositories/base.repository';
import { FoodHistoryEntity } from '@features/food-history/database/food-history.entity';
import { EntityRepository } from 'typeorm';
import { CompanyEntity } from '@features/company/database/company.entity';

@EntityRepository(FoodHistoryEntity)
export class FoodHistoryRepository extends AppBaseRepository<FoodHistoryEntity> {

  async groupByDateAndPrice(company: CompanyEntity): Promise<number> {
    const result: {price: number}[] = await this.query(`
        SELECT date_part('month', "history"."createdAt") AS month, sum("history"."price" * "history".count) as price
        FROM "food_history_entity" "history"
        WHERE "history"."companyId" = ${company.id}
        GROUP BY date_part('month', "history"."createdAt")
    `);

    return result.reduce((a, {price}) => +price + a, 0) / result.length;
  }
}
