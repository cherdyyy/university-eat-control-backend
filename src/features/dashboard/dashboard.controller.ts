import { Controller, Get, UseGuards } from '@nestjs/common';
import { DashboardModel } from '@features/dashboard/models/dashboard.model';
import { DashboardService } from '@features/dashboard/dashboard.service';
import { User } from '@shared/decorators/user.decorator';
import { UserEntity } from '@features/user/database/user.entity';
import { JwtAuthGuard } from '@features/auth/guards/jwt-auth.guard';

@UseGuards(JwtAuthGuard)
@Controller('dashboard')
export class DashboardController {

  constructor(private dashboardService: DashboardService) {
  }

  @Get()
  getDashboardInfo(
    @User() user: UserEntity
  ): Promise<DashboardModel> {
    return this.dashboardService.getDashboard(user);
  }
}
