import { BaseModel } from '@shared/models/base.model';
import { FoodEntity } from '@features/food/database/food.entity';

export class FoodModel extends BaseModel {
  name: string;
  description: string;
  price: number;
  calories: number;
  category: string;

  constructor(entity: Partial<FoodEntity> = {}) {
    super(entity);
    this.name = entity.name;
    this.description = entity.description;
    this.price = entity.price;
    this.calories = entity.calories;
    this.category = entity.category && entity.category.name;
  }
}
