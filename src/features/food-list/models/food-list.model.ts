import { FoodListEntity } from '@features/food-list/database/food-list.entity';
import { BaseModel } from '@shared/models/base.model';
import { FoodModel } from '@features/food/models/food.model';

export class FoodListModel extends BaseModel {
  food: FoodModel;
  count: number;

  constructor(entity: FoodListEntity) {
    super(entity);
    this.food = new FoodModel(entity.food || undefined);
    this.count = entity.count;
  }
}
