import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '@features/auth/guards/jwt-auth.guard';
import { FoodCategoryService } from '@features/food-category/food-category.service';
import { NameDto } from '@shared/dto/name.dto';
import { User } from '@shared/decorators/user.decorator';
import { UserEntity } from '@features/user/database/user.entity';
import { OptionListModel } from '@shared/models/option-list.model';

@UseGuards(JwtAuthGuard)
@Controller('food-category')
export class FoodCategoryController {

  constructor(private foodCategoryService: FoodCategoryService) {
  }

  @Get('list')
  async getCategoryList(@User() user: UserEntity): Promise<OptionListModel[]> {
    const list = await this.foodCategoryService.getFoodCategoryList(user);

    return list.map((c) => new OptionListModel(c.id, c.name));
  }

  @Post()
  async createFoodCategory(
    @Body() body: NameDto,
    @User() user: UserEntity
  ): Promise<OptionListModel> {
    const result = await this.foodCategoryService.createFoodCategory(body, user);
    return new OptionListModel(result.id, result.name);
  }
}
