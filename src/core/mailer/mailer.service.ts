import { Injectable } from '@nestjs/common';
import { ISendMailOptions, MailerService } from '@nestjs-modules/mailer';
import { SentMessageInfo } from 'nodemailer';
import {readFile} from 'fs-extra';
import * as path from 'path';

type TemplateName = 'sign-up' | 'report';

@Injectable()
export class AppMailerService {

  constructor(private mailerService: MailerService) {}

  sendMail(sendMailOptions: ISendMailOptions): Promise<SentMessageInfo> {
    return this.mailerService.sendMail(sendMailOptions);
  }

  static getTemplatePath(templateName: TemplateName): string {
    return `${path.dirname(require.main.filename)}/assets/templates/${templateName}.template.html`;
  }

  static addTemplateVariable<T>(variables: T, template: string): string {
    for (const key in variables) {
      if (variables.hasOwnProperty(key))
        template = template.replace(`{{${key}}}`, `${variables[key]}`);
    }

    return template;
  }

  static async getTemplate<T>(templateName: TemplateName, variables: T): Promise<string> {
    const template = await readFile(this.getTemplatePath(templateName), 'utf8');

    return this.addTemplateVariable(variables, template);
  }
}
