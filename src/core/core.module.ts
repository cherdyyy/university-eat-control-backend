import { Global, Module } from '@nestjs/common';
import { AppConfigModule } from '@core/config/config.module';
import { DatabaseModule } from './database/database.module';
import { AppMailerModule } from './mailer/mailer.module';

@Global()
@Module({
  imports: [
    AppConfigModule,
    DatabaseModule,
    AppMailerModule,
  ],
  exports: [
    AppConfigModule,
    AppMailerModule,
  ]
})
export class CoreModule {

}
