import { IsNotEmpty, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { IdDto } from '@shared/dto/id.dto';
import { FoodCategoryEntity } from '@features/food-category/database/food-category.entity';

export class FoodDto {

  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  description: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  calories: number;

  @IsNotEmpty()
  @ValidateNested()
  @Type(() => IdDto)
  category: FoodCategoryEntity;
}
