import { Body, Controller, Get, Patch, Post, UseGuards } from '@nestjs/common';
import { CompanyModel } from '@features/company/models/company.model';
import { CompanyService } from '@features/company/company.service';
import { User } from '@shared/decorators/user.decorator';
import { UserEntity } from '@features/user/database/user.entity';
import { JwtAuthGuard } from '@features/auth/guards/jwt-auth.guard';
import { CreateCompanyDto } from '@features/company/dto/create-company.dto';
import { StatusModel } from '@shared/models/status.model';
import { UpdateCompanyDto } from '@features/company/dto/update-company.dto';

@Controller('company')
export class CompanyController {

  constructor(private companyService: CompanyService) {

  }

  @Get()
  @UseGuards(JwtAuthGuard)
  async getCurrentCompany(@User() user: UserEntity): Promise<CompanyModel> {
    return new CompanyModel(user.company);
  }

  @Post()
  async createCompany(@Body() body: CreateCompanyDto): Promise<CompanyModel> {
    return new CompanyModel(await this.companyService.createCompany(body));
  }

  @Patch()
  @UseGuards(JwtAuthGuard)
  async updateCompany(@Body() body: UpdateCompanyDto, @User() user: UserEntity): Promise<StatusModel> {
    await this.companyService.updateCompany(body, user.company);
    return new StatusModel(true);
  }

  @Get('recommended-balance')
  @UseGuards(JwtAuthGuard)
  recommendedBalance(@User() user: UserEntity): Promise<number> {
    return this.companyService.getRecommendedBalance(user.company);
  }
}
