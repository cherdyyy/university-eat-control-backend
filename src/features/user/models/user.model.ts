import { UserEntity } from '@app/features/user/database/user.entity';
import { BaseModel } from '@shared/models/base.model';

export class UserModel extends BaseModel {
  login: string;
  firstName: string;
  lastName: string;
  address: string;
  phoneNumber: string;
  companyId: number;

  constructor(entity: Partial<UserEntity> = {}) {
    super(entity);
    this.login = entity.login;
    this.firstName =  entity.firstName;
    this.lastName =  entity.lastName;
    this.address =  entity.address;
    this.phoneNumber =  entity.phoneNumber;
    this.companyId = entity.company && entity.company.id;
  }
}
