import { AppBaseEntity } from '@shared/database/entities/base.entity';
import { Column, Entity, ManyToOne, OneToMany, RelationCount } from 'typeorm';
import { UserEntity } from '@features/user/database/user.entity';
import { FoodEntity } from '@features/food/database/food.entity';
import { ReportsEntity } from '@features/reports/database/reports.entity';
import { CompanyEntity } from '@features/company/database/company.entity';

@Entity()
export class FoodHistoryEntity extends AppBaseEntity {

  @ManyToOne(() => UserEntity, user => user.foodHistory)
  user: UserEntity;

  @ManyToOne(() => FoodEntity, food => food.foodHistory)
  food: FoodEntity;

  @Column()
  count: number;

  @Column({
    default: 0
  })
  price: number;

  @OneToMany(() => ReportsEntity, reports => reports.foodHistoryList)
  reports: ReportsEntity[];

  @ManyToOne(() => CompanyEntity, company => company.foodHistory)
  company: CompanyEntity;

  @RelationCount((history: FoodHistoryEntity) => history.reports)
  reportsCount: number;
}
