import { Module } from '@nestjs/common';
import { FoodListController } from './food-list.controller';
import { FoodListService } from './food-list.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FoodListRepository } from '@features/food-list/database/food-list.repository';

@Module({
  controllers: [
    FoodListController
  ],
  providers: [
    FoodListService
  ],
  imports: [
    TypeOrmModule.forFeature([FoodListRepository])
  ],
  exports: [
    FoodListService
  ]
})
export class FoodListModule {}
