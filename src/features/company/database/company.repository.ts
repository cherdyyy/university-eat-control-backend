import { AppBaseRepository } from '@shared/database/repositories/base.repository';
import { EntityRepository } from 'typeorm';
import { CompanyEntity } from '@features/company/database/company.entity';

@EntityRepository(CompanyEntity)
export class CompanyRepository extends AppBaseRepository<CompanyEntity> {

}
