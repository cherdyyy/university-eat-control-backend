import { IsNotEmpty, ValidateNested } from 'class-validator';
import { Type } from 'class-transformer';
import { ListProp } from '@shared/interfaces/list.interface';

export const getListDto = <T>(item: Function): ListProp<T> => {
  class ListDto implements ListProp<T> {

    @IsNotEmpty()
    @ValidateNested({ each: true })
    @Type(() => item)
    list: T[];
  }

  return ListDto as any;
};
