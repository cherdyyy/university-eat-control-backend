import { IsNumber } from 'class-validator';
import { PlanEntity } from '@features/plan/database/plan.entity';

export class UpdateCompanyDto {

  @IsNumber()
  plan: PlanEntity;
}
