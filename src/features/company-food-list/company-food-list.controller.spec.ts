import { Test, TestingModule } from '@nestjs/testing';
import { CompanyFoodListController } from './company-food-list.controller';

describe('CompanyFoodListController', () => {
  let controller: CompanyFoodListController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [CompanyFoodListController],
    }).compile();

    controller = module.get<CompanyFoodListController>(CompanyFoodListController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
