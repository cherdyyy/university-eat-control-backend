import { IsEmail, IsNotEmpty, IsPhoneNumber, IsString, MinLength } from 'class-validator';
import { CompanyEntity } from '@features/company/database/company.entity';

export class SignUpDto {

  @IsString()
  @MinLength(3)
  login: string;

  @IsString()
  @MinLength(6)
  password: string;

  @IsString()
  @IsNotEmpty()
  firstName: string;

  @IsNotEmpty()
  @IsString()
  lastName: string;

  @IsString()
  @IsNotEmpty()
  address: string;

  @IsPhoneNumber(null)
  phoneNumber: string;

  @IsEmail()
  email: string;

  @IsNotEmpty()
  company: CompanyEntity;
}
