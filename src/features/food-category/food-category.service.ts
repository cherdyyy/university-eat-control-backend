import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FoodCategoryRepository } from '@features/food-category/database/food-category.repository';
import { FoodCategoryEntity } from '@features/food-category/database/food-category.entity';
import { NameDto } from '@shared/dto/name.dto';
import { UserEntity } from '@features/user/database/user.entity';

@Injectable()
export class FoodCategoryService {

  constructor(@InjectRepository(FoodCategoryRepository)
              private foodRepository: FoodCategoryRepository) {
  }

  createFoodCategory(body: NameDto, user: UserEntity): Promise<FoodCategoryEntity> {
    return this.foodRepository.save({
      ...body,
      company: user.company
    })
  }

  getFoodCategoryList(user: UserEntity): Promise<FoodCategoryEntity[]> {
    return this.foodRepository.find({
      company: user.company
    });
  }
}
