import { EmployeesListModel } from '@features/employee/models/employees-list.model';
import { UserEntity } from '@features/user/database/user.entity';

export class EmployeeModel extends EmployeesListModel {

  constructor(entity: UserEntity) {
    super(entity);
  }
}
