import { Test, TestingModule } from '@nestjs/testing';
import { FoodListController } from './food-list.controller';

describe('FoodListController', () => {
  let controller: FoodListController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FoodListController],
    }).compile();

    controller = module.get<FoodListController>(FoodListController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
