import { Module } from '@nestjs/common';
import { AuthModule } from '@app/features/auth/auth.module';
import { UserModule } from './user/user.module';
import { FoodModule } from './food/food.module';
import { PlanModule } from './plan/plan.module';
import { CompanyModule } from './company/company.module';
import { EmployeeModule } from './employee/employee.module';
import { FoodHistoryModule } from './food-history/food-history.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { FoodListModule } from './food-list/food-list.module';
import { CompanyFoodListModule } from './company-food-list/company-food-list.module';
import { FoodCategoryModule } from './food-category/food-category.module';
import { ReportsModule } from './reports/reports.module';

const FEATURES = [
  PlanModule,
  CompanyModule,
  AuthModule,
  UserModule,
  FoodModule,
  EmployeeModule,
  FoodHistoryModule,
  DashboardModule,
  FoodListModule,
  CompanyFoodListModule,
];

@Module({
  imports: [
    ...FEATURES,
    FoodCategoryModule,
    ReportsModule,
  ],
  exports: [
    ...FEATURES
  ]
})
export class FeaturesModule {}
