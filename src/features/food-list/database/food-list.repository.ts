import { AppBaseRepository } from '@shared/database/repositories/base.repository';
import { EntityRepository } from 'typeorm';
import { FoodListEntity } from '@features/food-list/database/food-list.entity';
import { UserEntity } from '@features/user/database/user.entity';

@EntityRepository(FoodListEntity)
export class FoodListRepository extends AppBaseRepository<FoodListEntity> {

  getAllUserFood(user: UserEntity): Promise<FoodListEntity[]> {
    return this.createQueryBuilder('food_list_entity')
      .where({user})
      .leftJoinAndSelect('food_list_entity.food', 'food')
      .getMany();
  }
}
