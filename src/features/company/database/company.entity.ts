import { AppBaseEntity } from '@app/shared/database/entities/base.entity';
import { PlanEntity } from '@app/features/plan/database/plan.entity';
import { Column, Entity, ManyToOne, OneToMany, RelationCount } from 'typeorm';
import { UserEntity } from '@features/user/database/user.entity';
import { CompanyFoodListEntity } from '@features/company-food-list/database/company-food-list.entity';
import { FoodCategoryEntity } from '@features/food-category/database/food-category.entity';
import { ReportsEntity } from '@features/reports/database/reports.entity';
import { FoodHistoryEntity } from '@features/food-history/database/food-history.entity';

@Entity()
export class CompanyEntity extends AppBaseEntity {

  @ManyToOne(() => PlanEntity)
  plan: PlanEntity;

  @Column()
  name: string;

  @Column()
  address: string;

  @Column({
    default: 0
  })
  balance: number;

  @OneToMany(() => UserEntity, user => user.company)
  user: UserEntity[];

  @OneToMany(() => CompanyFoodListEntity, list => list.company)
  foodList: CompanyFoodListEntity;

  @OneToMany(() => FoodCategoryEntity, list => list.company)
  foodCategories: FoodCategoryEntity;

  @OneToMany(() => ReportsEntity, reports => reports.company)
  reports: ReportsEntity;

  @OneToMany(() => FoodHistoryEntity, reports => reports.company)
  foodHistory: FoodHistoryEntity;

  @RelationCount((company: CompanyEntity) => company.user)
  employeesCount: number;
}
