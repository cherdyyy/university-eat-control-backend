import { Injectable } from '@nestjs/common';
import { DashboardModel } from '@features/dashboard/models/dashboard.model';
import { InjectRepository } from '@nestjs/typeorm';
import { FoodHistoryRepository } from '@features/food-history/database/food-history.repository';
import { UserEntity } from '@features/user/database/user.entity';

@Injectable()
export class DashboardService {

  constructor(@InjectRepository(FoodHistoryRepository)
              private historyRepository: FoodHistoryRepository) {
  }

  async getDashboard(
    user: UserEntity
  ): Promise<DashboardModel> {
    const history = await this.historyRepository.find({
      where: {
        company: user.company
      },
      relations: [
        'user', 'food', 'reports'
      ]
    });

    return new DashboardModel(
      history
    );
  }
}
