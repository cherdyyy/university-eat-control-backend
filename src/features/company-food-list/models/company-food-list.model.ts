import { BaseModel } from '@shared/models/base.model';
import { CompanyFoodListEntity } from '@features/company-food-list/database/company-food-list.entity';
import { FoodModel } from '@features/food/models/food.model';

export class CompanyFoodListModel extends BaseModel {
  food: FoodModel;
  count: number;

  constructor(entity: CompanyFoodListEntity) {
    super(entity);
    this.food = new FoodModel(entity.food || undefined);
    this.count = entity.count;
  }
}
