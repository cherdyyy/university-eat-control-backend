import { Global, Module } from '@nestjs/common';
import { CompanyService } from './company.service';
import { CompanyController } from './company.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CompanyRepository } from '@features/company/database/company.repository';

@Global()
@Module({
  providers: [
    CompanyService
  ],
  controllers: [
    CompanyController
  ],
  imports: [
    TypeOrmModule.forFeature([CompanyRepository]),
  ],
  exports: [
    TypeOrmModule,
    CompanyService
  ]
})
export class CompanyModule {}
