export enum FoodListResponseTextEnum {
  FOOD_ITEM_NOT_FOUND = 'Cannot find food item'
}
