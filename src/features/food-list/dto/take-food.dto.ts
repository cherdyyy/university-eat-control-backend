import { IsNotEmpty, IsNumber, ValidateNested } from 'class-validator';
import { IdDto } from '@shared/dto/id.dto';
import { Type } from 'class-transformer';
import { FoodEntity } from '@features/food/database/food.entity';

export class TakeFoodDto {
  @IsNotEmpty()
  @IsNumber()
  count: number;

  @IsNotEmpty()
  @ValidateNested()
  @Type(() => IdDto)
  food: FoodEntity;
}
