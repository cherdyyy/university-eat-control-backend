import { Test, TestingModule } from '@nestjs/testing';
import { FoodHistoryController } from './food-history.controller';

describe('FoodHistoryController', () => {
  let controller: FoodHistoryController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [FoodHistoryController],
    }).compile();

    controller = module.get<FoodHistoryController>(FoodHistoryController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
