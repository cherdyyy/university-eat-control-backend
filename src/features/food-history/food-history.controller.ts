import { Body, Controller, Get, Post } from '@nestjs/common';
import { FoodHistoryService } from '@features/food-history/food-history.service';
import { FindAllDto, Where, WhereTypeEnum } from '@shared/dto/find-all.dto';
import { FoodHistoryEntity } from '@features/food-history/database/food-history.entity';
import { User } from '@shared/decorators/user.decorator';
import { UserEntity } from '@features/user/database/user.entity';
import { FindAllModel } from '@shared/models/find-all.model';
import { FoodHistoryModel, FullFoodHistoryModel } from '@features/food-history/models/food-history.model';

@Controller('food-history')
export class FoodHistoryController {

  constructor(private foodHistoryService: FoodHistoryService) {
  }

  @Post()
  async getAllFoodHistory(
    @Body() query: FindAllDto<FoodHistoryEntity>,
  ): Promise<FindAllModel<FullFoodHistoryModel>> {
    const list = await this.foodHistoryService.getAllFoodHistory(
      query,
      ['food', 'user']
    );

    return {
      ...list,
      items: list.items.map(item => new FullFoodHistoryModel(item))
    }
  }

  @Get('mine')
  async getFoodHistoryByUser(
    @Body() query: FindAllDto<FoodHistoryEntity>,
    @User() user: UserEntity
  ): Promise<FindAllModel<FoodHistoryModel>> {
    query.where.push(
      new Where('user', user, WhereTypeEnum.SIMPLE)
    );
    const list = await this.foodHistoryService.getAllFoodHistory(
      query,
      ['food']
    );

    return {
      ...list,
      items: list.items.map(item => new FoodHistoryModel(item))
    }
  }
}
