import { Global, Module } from '@nestjs/common';
import { FoodService } from './food.service';
import { FoodController } from './food.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FoodRepository } from '@features/food/database/food.repository';

@Global()
@Module({
  providers: [
    FoodService
  ],
  controllers: [
    FoodController
  ],
  imports: [
    TypeOrmModule.forFeature([FoodRepository]),
  ],
  exports: [
    TypeOrmModule,
    FoodService
  ]
})
export class FoodModule {}
