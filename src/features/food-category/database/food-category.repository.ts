import { EntityRepository } from 'typeorm';
import { AppBaseRepository } from '@shared/database/repositories/base.repository';
import { FoodCategoryEntity } from '@features/food-category/database/food-category.entity';

@EntityRepository(FoodCategoryEntity)
export class FoodCategoryRepository extends AppBaseRepository<FoodCategoryEntity> {

}
