import { Entity, Column, ManyToOne, OneToMany } from 'typeorm';
import { AppBaseEntity } from '@app/shared/database/entities/base.entity';
import { UserTypeEnum } from '@app/features/user/enums/user-type.enum';
import { CompanyEntity } from '@features/company/database/company.entity';
import { FoodHistoryEntity } from '@features/food-history/database/food-history.entity';
import { FoodListEntity } from '@features/food-list/database/food-list.entity';

@Entity()
export class UserEntity extends AppBaseEntity {

  @Column({
    nullable: true
  })
  login: string;

  @Column()
  password: string;

  @ManyToOne(() => CompanyEntity, company => company.user)
  company: CompanyEntity;

  @Column()
  firstName: string;

  @Column()
  lastName: string;

  @Column()
  address: string;

  @Column()
  phoneNumber: string;

  @Column()
  email: string;

  @Column('enum', {
    enum: UserTypeEnum,
    default: UserTypeEnum.Admin
  })
  type: UserTypeEnum;

  @OneToMany(() => FoodHistoryEntity, history => history.food)
  foodHistory: FoodHistoryEntity[];

  @OneToMany(() => FoodListEntity, list => list.user)
  foodList: FoodListEntity[];
}
