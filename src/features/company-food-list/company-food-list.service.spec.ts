import { Test, TestingModule } from '@nestjs/testing';
import { CompanyFoodListService } from './company-food-list.service';

describe('CompanyFoodListService', () => {
  let service: CompanyFoodListService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CompanyFoodListService],
    }).compile();

    service = module.get<CompanyFoodListService>(CompanyFoodListService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
