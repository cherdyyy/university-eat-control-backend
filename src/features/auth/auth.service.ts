import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { SignUpDto } from '@app/features/auth/dto/sign-up.dto';
import { SignInDto } from '@app/features/auth/dto/sign-in.dto';
import { compare} from 'bcrypt'
import { TokenModel } from '@app/features/auth/models/token.model';
import { TokenContextModel } from '@app/features/auth/models/token-context.model';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from '@features/user/database/user.repository';
import { UserService } from '@features/user/user.service';
import { AuthResponseTextEnum } from '@features/auth/enums/response-text.enum';
import { AppMailerService } from '@core/mailer/mailer.service';
import { SignUpTemplate } from '@core/mailer/interfaces/sign-up.interface';

@Injectable()
export class AuthService {

  constructor(private jwtService: JwtService,
              @InjectRepository(UserRepository)
              private usersRepository: UserRepository,
              private userService: UserService,
              private mailerService: AppMailerService) {
  }

  async signUp(body: SignUpDto): Promise<TokenModel> {
    const user = await this.usersRepository.findOne({
      email: body.email,
      login: body.login
    });

    if (user) {
      throw new HttpException(
        AuthResponseTextEnum.USER_EXIST,
        HttpStatus.CONFLICT
      );
    }

    const newUser = await this.userService.createUser(body);

    return new TokenModel(await this.jwtService.signAsync({
      ...new TokenContextModel(newUser.id)
    }));
  }

  async signIn(body: SignInDto): Promise<TokenModel> {
    const user = await this.usersRepository.findOne({
      email: body.email
    });

    if (!user) {
      throw new HttpException(
        AuthResponseTextEnum.LOGIN_NOT_FOUND,
        HttpStatus.UNAUTHORIZED
      );
    }

    if (!(await compare(body.password, user.password))) {
      throw new HttpException(
        AuthResponseTextEnum.INCORRECT_PASSWORD,
        HttpStatus.UNAUTHORIZED
      );
    }

    return new TokenModel(await this.jwtService.signAsync({
      ...new TokenContextModel(user.id)
    }));
  }

  async sendUserCreateMail(variables: SignUpTemplate): Promise<void> {
    return this.mailerService.sendMail({
      html: await AppMailerService.getTemplate<SignUpTemplate>('sign-up', variables),
      to: variables.email,
      subject: 'Account Credentials',
    });
  }
}
