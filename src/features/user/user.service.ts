import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from '@app/features/user/database/user.repository';
import { UserEntity } from '@features/user/database/user.entity';
import { hash } from 'bcrypt';
import { CompanyEntity } from '@features/company/database/company.entity';
import { UserTypeEnum } from '@features/user/enums/user-type.enum';

@Injectable()
export class UserService {

  constructor(@InjectRepository(UserRepository)
              private usersRepository: UserRepository) {
  }

  async createUser(body: Partial<UserEntity>): Promise<UserEntity> {
    return this.usersRepository.insertAndReturnOne({
      ...body,
      password: await hash(body.password, 10),
    });
  }

  async getAdmins(company: CompanyEntity): Promise<UserEntity[]> {
    return this.usersRepository.find({
      company: company,
      type: UserTypeEnum.Admin
    })
  }
}
