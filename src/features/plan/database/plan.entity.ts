import { AppBaseEntity } from '@app/shared/database/entities/base.entity';
import { Column, Entity, OneToMany } from 'typeorm';
import { CompanyEntity } from '@features/company/database/company.entity';

@Entity()
export class PlanEntity extends AppBaseEntity {
  @Column()
  name: string;

  @Column()
  description: string;

  @Column({
    default: 0
  })
  price: number;

  @OneToMany(() => CompanyEntity, company => company.id)
  company: CompanyEntity;

  @Column({
    default: false,
  })
  isDefault: boolean;
}
