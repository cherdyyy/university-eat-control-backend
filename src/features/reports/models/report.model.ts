import { ReportsEntity } from '@features/reports/database/reports.entity';

export class ReportModel {
  fullName: string;
  foodName: string;
  count: number;

  constructor(report: ReportsEntity) {
    this.fullName = `${report.foodHistoryList.user.firstName} ${report.foodHistoryList.user.lastName}`;
    this.foodName = report.foodHistoryList.food.name;
    this.count = report.foodHistoryList.count;
  }
}
