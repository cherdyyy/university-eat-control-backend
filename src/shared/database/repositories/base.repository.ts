import { DeepPartial, LessThan, Like, MoreThan, Repository } from 'typeorm';
import { PaginationModel } from '@shared/models/pagination.model';
import { FindAllDto, WhereTypeEnum } from '@shared/dto/find-all.dto';
import { FindAllModel } from '@shared/models/find-all.model';

export class AppBaseRepository<T> extends Repository<T> {

  async insertAndReturnOne(entity: DeepPartial<T>): Promise<T> {
    const result = await this.insert(this.create(entity));

    const [{id}] = result.identifiers;

    return this.findOne({where: {id}});
  }

  async findAll(query: FindAllDto<T>, relations: string[] = []): Promise<FindAllModel<T>> {
    let where = {};

    if (Array.isArray(query.where)) {
      for (const item of query.where) {
        switch (item.type) {
          case WhereTypeEnum.LIKE:
            where[item.key] = Like(item.value);
            break;
          case WhereTypeEnum.SIMPLE:
            where[item.key] = item.value;
            break;
          case WhereTypeEnum.LESS_THAN:
            where[item.key] = LessThan(item.value);
            break
          case WhereTypeEnum.MORE_THAN:
            where[item.key] = MoreThan(item.value);
            break
        }
      }
    }

    where = this.create(where);

    const [items, total] = await this.findAndCount(
      {
        where,
        take: query.pageSize,
        skip: query.page * query.pageSize,
        order: query.order,
        relations
      }
    );
    const totalPages = Math.ceil(total / query.pageSize)

    return new FindAllModel(
      items,
      new PaginationModel(
        totalPages ? totalPages - 1 : 0,
        query.page,
        query.pageSize,
        total
      )
    );
  }
}
