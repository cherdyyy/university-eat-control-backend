import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FoodHistoryRepository } from '@features/food-history/database/food-history.repository';
import { FoodHistoryEntity } from '@features/food-history/database/food-history.entity';
import { FindAllDto } from '@shared/dto/find-all.dto';
import { FindAllModel } from '@shared/models/find-all.model';
import { CompanyEntity } from '@features/company/database/company.entity';

@Injectable()
export class FoodHistoryService {

  constructor(@InjectRepository(FoodHistoryRepository)
              private foodRepository: FoodHistoryRepository) {
  }

  addHistoryItem(entity: Partial<FoodHistoryEntity>): Promise<FoodHistoryEntity> {
    return this.foodRepository.insertAndReturnOne(entity);
  }

  async getAllFoodHistory(
    query: FindAllDto<FoodHistoryEntity>,
    relations: string[] = []
  ): Promise<FindAllModel<FoodHistoryEntity>> {
    return this.foodRepository.findAll(query, relations);
  }

  async getRecommendedBalance(company: CompanyEntity): Promise<number> {
    return  await this.foodRepository.groupByDateAndPrice(company);
  }
}
