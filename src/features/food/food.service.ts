import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { FoodEntity } from '@features/food/database/food.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { FoodRepository } from '@features/food/database/food.repository';
import { FoodDto } from '@features/food/dto/food.dto';
import { StatusModel } from '@shared/models/status.model';
import { CompanyEntity } from '@features/company/database/company.entity';
import { FindAllModel } from '@shared/models/find-all.model';
import { FindAllDto } from '@shared/dto/find-all.dto';
import { IdDto } from '@shared/dto/id.dto';
import { UpdateResult } from 'typeorm';

@Injectable()
export class FoodService {

  constructor(@InjectRepository(FoodRepository)
              private foodRepository: FoodRepository) {
  }

  createFood(body: FoodDto, company: CompanyEntity): Promise<FoodEntity> {
    if (!company) {
      throw new HttpException(
        'Firstly you must create the company!',
        HttpStatus.CONFLICT
      );
    }

    return this.foodRepository.insertAndReturnOne({
      ...body,
      company
    });
  }

  getFoodById(id: number): Promise<FoodEntity> {
    return this.foodRepository.findOne(id, {
      relations: ['category']
    });
  }

  async updateFoodPartial(body: IdDto): Promise<UpdateResult> {
    const {id, ...rest} = body;
    return await this.foodRepository.update(id, rest);
  }

  async deleteProduct(id: number): Promise<StatusModel> {
    await this.foodRepository.delete(id);
    return new StatusModel(true);
  }

  async getFoodListPagination(query: FindAllDto<FoodEntity>): Promise<FindAllModel<FoodEntity>> {
    return this.foodRepository.findAll(query, ['category', 'foodList']);
  }

  async getFoodList(): Promise<FoodEntity[]> {
    return this.foodRepository.find({});
  }
}
