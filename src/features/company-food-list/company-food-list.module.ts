import { Global, Module } from '@nestjs/common';
import { CompanyFoodListService } from './company-food-list.service';
import { CompanyFoodListController } from './company-food-list.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CompanyFoodListRepository } from '@features/company-food-list/database/company-food-list.repository';

@Global()
@Module({
  providers: [
    CompanyFoodListService
  ],
  controllers: [
    CompanyFoodListController
  ],
  imports: [
    TypeOrmModule.forFeature([CompanyFoodListRepository])
  ],
  exports: [
    CompanyFoodListService
  ]
})
export class CompanyFoodListModule {}
