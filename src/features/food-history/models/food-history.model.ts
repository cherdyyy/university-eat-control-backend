import { BaseModel } from '@shared/models/base.model';
import { FoodHistoryEntity } from '@features/food-history/database/food-history.entity';

export class FoodHistoryModel extends BaseModel {
  foodName: string;
  count: number;

  constructor(entity: FoodHistoryEntity) {
    super(entity);
    this.foodName = entity.food && entity.food.name;
    this.count = entity.count;
  }
}

export class FullFoodHistoryModel extends FoodHistoryModel {
  username: string;

  constructor(entity: FoodHistoryEntity) {
    super(entity);
    this.username = `${entity.user.firstName} ${entity.user.lastName}`;
  }
}
