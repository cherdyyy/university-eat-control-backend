import { IsNotEmpty, ValidateNested } from 'class-validator';
import { IdDto } from '@shared/dto/id.dto';
import { Type } from 'class-transformer';
import { FoodEntity } from '@features/food/database/food.entity';
import { ListProp } from '@shared/interfaces/list.interface';


export class CompanyFoodListDto implements ListProp<CompanyFoodItemDto> {

  @IsNotEmpty()
  @ValidateNested({each: true})
  @Type(() => CompanyFoodItemDto)
  list: CompanyFoodItemDto[];
}

export class CompanyFoodItemDto {
  @ValidateNested()
  @Type(() => IdDto)
  food: FoodEntity;

  @IsNotEmpty()
  count: number;
}
