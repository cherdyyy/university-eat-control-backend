import { Column, Entity, ManyToOne } from 'typeorm';
import { AppBaseEntity } from '@shared/database/entities/base.entity';
import { CompanyEntity } from '@features/company/database/company.entity';
import { FoodEntity } from '@features/food/database/food.entity';

@Entity()
export class FoodCategoryEntity extends AppBaseEntity {

  @Column()
  name: string;

  @ManyToOne(() => CompanyEntity)
  company: CompanyEntity;

  @ManyToOne(() => FoodEntity, food => food.category)
  food: FoodEntity;
}
