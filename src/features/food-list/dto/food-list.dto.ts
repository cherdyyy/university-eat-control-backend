import { IsNotEmpty, ValidateNested } from 'class-validator';
import { IdDto } from '@shared/dto/id.dto';
import { Type } from 'class-transformer';
import { FoodEntity } from '@features/food/database/food.entity';
import { ListProp } from '@shared/interfaces/list.interface';
import { UserProp } from '@shared/interfaces/user.interface';
import { UserEntity } from '@features/user/database/user.entity';

export class FoodListDto implements ListProp<FoodItemDto>, UserProp<UserEntity> {

  @IsNotEmpty()
  @ValidateNested({each: true})
  @Type(() => FoodItemDto)
  list: FoodItemDto[];

  @IsNotEmpty()
  @ValidateNested()
  @Type(() => IdDto)
  user: UserEntity;
}

export class FoodItemDto {
  @IsNotEmpty()
  @ValidateNested()
  @Type(() => IdDto)
  food: FoodEntity;

  @IsNotEmpty()
  count: number;
}
