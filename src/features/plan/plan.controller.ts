import { Controller, Get } from '@nestjs/common';
import { PlanService } from '@features/plan/plan.service';
import { PlanModel } from '@features/plan/models/plan.model';

@Controller('plan')
export class PlanController {

  constructor(private planService: PlanService) {}

  @Get()
  async getPlans(): Promise<PlanModel[]> {
    const plans = await this.planService.getPlans();
    return plans.map(plan => new PlanModel(plan));
  }
}
